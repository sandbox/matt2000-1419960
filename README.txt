Defines a particular Webform node as a 'template' and allows other content
types' nodes to be desginated as the 'owner' of a (possibly modified) clone of
the template, which can be created automatically during the creation of the
owner node.

Possible use cases:

- An RSVP template form which is owned by Event nodes.
- An Contact Information form which is owned by 'Directory Listing' nodes.
   (e.g., for Lead Generation)
   