<?php
// $Id: clone.module,v 1.9.2.14 2008/10/15 18:38:49 pwolanin Exp $
// $Name: DRUPAL-5--2-6 $

/**
 * @file
 * Based on node clone module
 */

function _webform_ez_clone_access($node) {
  global $user;
  // Check basic permissions first.
  $access = (user_access('clone node') || ($user->uid && ($node->uid == $user->uid) && user_access('clone own nodes')));
  // Make sure the user can view the original node content.
  $access = $access && node_access('view', $node);
  // Check additional conditions
  $access = $access && filter_access($node->format) && node_access('create', $node->type);
  // Let other modules alter this - for exmple to only allow some users
  // to clone specific nodes or types.
  foreach (module_implements('clone_access_alter') as $module) {
    $function = $module . '_clone_access_alter';
    $function($access, $node);
  }
  return $access;
}


/**
 * Clones a node - prepopulate a node object
 *
 * @param $nid (int) node id
 */
function webform_ez_clone_node($nid, $title = 'Survey') {
  if (is_numeric($nid)) {
    global $user;

    $node = node_load($nid);
    if (isset($node->nid)) {
      $original_node = clone $node;

      $node->nid = NULL;
      $node->vid = NULL;
      $node->name = $user->name;
      $node->uid = $user->uid;
      $node->created = NULL;
      $node->menu = NULL;
      $node->path = NULL;
      $node->files = array();
      // Remove CCK file and image attachements
      if (module_exists('imagefield') || module_exists('filefield')) {
        $content_type = module_invoke('content', 'types', $node->type);
        // Find all the fields that are files or images.
        foreach ($content_type['fields'] as $data) {
          if (($data['type'] == 'file') || ($data['type'] == 'image')) {
            $key = $data['field_name'];
            // Remove any attached files as with $node->files
            if (isset($node->$key)) {
              $node->$key = array();
            }
          }
        }
      }
      // Add indicator text to the title.
      $node->title = t(check_plain($title));

      // Let other modules do special fixing up.
      // The function signature is: hook_clone_node_alter(&$node, $original_node, $method)
      foreach (module_implements('clone_node_alter') as $module) {
        $function = $module . '_clone_node_alter';
        $function($node, $original_node, "prepopulate");
      }
      return  $node;
    }
  }
}
